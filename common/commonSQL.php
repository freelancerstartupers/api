<?php
/*
    @return bool
*/
function insert($table, $params)
{
    $chooses = "(";
    $contents = "(";
    $numItems = count($params);
    $i = 0;
    foreach ($params as $key => $value) {
        if (++$i === $numItems) {
            $chooses .= "`$key`)";
            $contents .= "'$value')";
        } else {
            $chooses .= "`$key`, ";
            $contents .= "'$value', ";
        }
    }

    return write("insert into `$table` $chooses values $contents");
}

/*
    @return bool
*/
function update($table, $params, $conditions)
{
    $chooses = "";
    $contents = "";
    $numItems = count($params);
    $i = 0;
    foreach ($params as $key => $value) {
        if (++$i === $numItems) {
            $contents .= "`$key` = '$value'";
        } else {
            $contents .= "`$key` = '$value', ";
        }
    }
    $numItems = count($conditions);
    $i = 0;
    foreach ($conditions as $key => $value) {
        if (++$i === $numItems) {
            $chooses .= "`$key` = '$value'";
        } else {
            $chooses .= "`$key` = '$value' and ";
        }
    }

    return write("update `$table` set $contents where $chooses");
}

/*
    @params $delete: check if want to delete in database or just update isdelete
    @return bool
*/
function delete($table, $params, $delete)
{
    $chooses = "";
    $contents = "";
    $numItems = count($params);
    $i = 0;
    foreach ($params as $key => $value) {
        if (++$i === $numItems) {
            $contents .= "`$key` = '$value'";
        } else {
            $contents .= "`$key` = '$value' and ";
        }
    }
    if ($delete === false) {
        return write("update `$table` set `isdeleted` = 1 where $contents");
    }
    return write("delete from `$table` where $contents");
}
