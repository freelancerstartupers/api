<?php
require_once './database/db.php';
require_once './common/commonSQL.php';
require_once './restfulapi/restful_api.php';

class api extends restful_api
{
    
    function __construct()
    {
        parent::__construct();
    }

    function responseData($status, $data, $msg)
    {
        $this->response(200, ['status'=> $status, 'data'=> $data, 'msg'=> $msg]);
    }

    function authen()
    {
        include_once './tables/authen.inc.php';
    }

    function taikhoan()
    {
        include_once './tables/taikhoan.inc.php';
    }

    function loaisanpham()
    {
        include_once './tables/loaisanpham.inc.php'; //delete
    }

    function haumai()
    {
        include_once './tables/haumai.inc.php';
    }

    function chitiethoadon()
    {
        include_once './tables/chitiethoadon.inc.php';
    }

    function hoadon()
    {
        include_once './tables/hoadon.inc.php'; //delete
    }

    function baiviet()
    {
        include_once './tables/baiviet.inc.php';
    }

    function sanpham()
    {
        include_once './tables/sanpham.inc.php'; //delete
    }

    function hinh()
    {
        include_once './tables/hinh.inc.php'; //delete
    }
}

$user_api = new api();
