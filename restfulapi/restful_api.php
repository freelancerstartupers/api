<?php

class restful_api
{
    /**
     * Property: $method
     * Method được gọi, GET POST PUT hoặc DELETE
     */
    protected $method = '';
    /**
     * Property: $endpoint
     * Endpoint của api
     */
    protected $endpoint = '';
    /**
     * Property: $params
     * Các tham số khác sau endpoint, ví dụ /<endpoint>/<param1>/<param2>
     */
    protected $params = array();

    protected $urlHost = '';
    /**
     * Function: __construct
     * Just a constructor
     */
    public function __construct()
    {
        $this->_input();
        $this->_process_api();
    }

    /**
     * Allow CORS
     * Thực hiện lấy các thông tin của request: endpoint, params và method
     */
    private function _input()
    {
        
        header('Access-Control-Allow-Origin: ' . $_SERVER['HTTP_ORIGIN']);
        header('Access-Control-Allow-Methods: GET, POST, DELETE, OPTIONS');
        header('Access-Control-Max-Age: 1000');
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');

        if (strpos($_SERVER['HTTP_ORIGIN'], "admin.")) {
            if ($_SERVER['REQUEST_URI'] != '/authen') {
                if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW'])) {
                    $tenhienthi = $_SERVER['PHP_AUTH_USER'];
                    
                    $matkhau =  $_SERVER['PHP_AUTH_PW'];
                    $sql = "select `id`, `tenhienthi`, `matkhau`, `vaitro`
                            from `taikhoan`
                            where `tenhienthi` = '$tenhienthi' and `matkhau` = '$matkhau'";
                    $list = load($sql);
                    if (!$list) {
                         $this->response(405);
                    }
                }
            }
        }

        $this->urlHost = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]/";

        $this->params   = explode('/', trim($_SERVER['REDIRECT_URL'], '/'));

        //The array_shift() function removes the first element from an array, and returns the value of
        //the removed element.
        $this->endpoint = array_shift($this->params);

        // Lấy method của request
        $method         = $_SERVER['REQUEST_METHOD'];
        $allow_method   = array('GET', 'POST', 'DELETE', 'OPTIONS');

        if (in_array($method, $allow_method)) {
            $this->method = $method;
        }
        
        // Nhân thêm dữ liệu tương ứng theo từng loại method
        switch ($this->method) {
            case 'OPTIONS':
                // Không cần nhận, bởi params đã được lấy từ url
                break;
                
            case 'POST':
                $this->params = array_merge($this->params, $_POST);
                break;

            case 'GET':
                // Không cần nhận, bởi params đã được lấy từ url
                break;

            case 'DELETE':
                // Không cần nhận, bởi params đã được lấy từ url
                break;

            default:
                $this->response(500, "Invalid Method");
                break;
        }
    }

    /**
     * Thực hiện xử lý request
     */
    private function _process_api()
    {
        if (method_exists($this, $this->endpoint)) {
            $this->{$this->endpoint}();
        } else {
            $this->response(500, "Unknown endpoint");
        }
    }

    /**
     * Trả dữ liệu về client
     * @param: $status_code: mã http trả về
     * @param: $data: dữ liệu trả về
     */
    protected function response($status_code, $data = null)
    {
        header($this->_build_http_header_string($status_code));
        header("Content-Type: application/json");
        echo json_encode($data);
        die();
    }

    /**
     * Tạo chuỗi http header
     * @param: $status_code: mã http
     * @return: Chuỗi http header, ví dụ: HTTP/1.1 404 Not Found
     */
    private function _build_http_header_string($status_code)
    {
        $status = array(
            200 => 'OK',
            404 => 'Not Found',
            405 => 'Method Not Allowed',
            500 => 'Internal Server Error'
        );
        return "HTTP/1.1 " . $status_code . " " . $status[$status_code];
    }
}
