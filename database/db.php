<?php
define("HOST", "localhost");
define("DB", "creatorvn");
define("UID", "root");
define("PWD", "");
// define("HOST", "localhost");
// define("DB", "creatorv_rp520");
// define("UID", "creatorv_rp520");
// define("PWD", "creatorv_rp520");

function load($sql)
{
    $cn = new mysqli(HOST, UID, PWD, DB);
    if ($cn->connect_errno) {
        die("FAILED TO CONNECT HOST");
    }
    $cn->query("set names 'utf8'");
    $rs = $cn->query($sql);
    $list = [];
    if ($rs) {
        while ($row = $rs->fetch_assoc()) {
            array_push($list, $row);
        }
    }

    return $list;
}

function write($sql)
{
    $cn = new mysqli(HOST, UID, PWD, DB);
    if ($cn->connect_errno) {
        die("FAILED TO CONNECT HOST");
    }
    $cn->query("set names 'utf8'");
    $cn->query($sql);

    return $cn->insert_id;
}
