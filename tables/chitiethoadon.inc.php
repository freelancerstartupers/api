<?php
switch ($this->method) {
    case 'GET':
        $sql = "select `id`, `mahd`, `masp`, `gia`, `soluong` 
                from `chitiethoadon`";
        $list = load($sql);

        $this->responseData(true, $list, 'get list of bill details');
        break;

    case 'POST':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $insert_id = insert('chitiethoadon', $this->params);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to insert a bill detail');
            } else {
                $this->responseData(false, null, 'fail to insert a bill detail');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('chitiethoadon', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to update a bill detail');
            } else {
                $this->responseData(false, null, 'fail to update a bill detail');
            }
        }
    
        break;

    case 'PUT':
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('chitiethoadon', ['id' => $id], true);
            $this->responseData(true, null, 'success to delete a bill detail');
        } else {
            $this->responseData(false, null, 'missing params');
        }
    
        break;
}
