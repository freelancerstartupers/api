<?php
switch ($this->method) {
    case 'GET':
        $sql = "select `id`, `mahm`, `phantram`, `dasudung`, `ngaykt` 
                from `haumai`";
        $list = load($sql);

        $this->responseData(true, $list, 'get list of discounts');
        break;

    case 'POST':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $this->params['mahm'] = crypt(uniqid(rand(), true), uniqid(rand(), true));
            $insert_id = insert('haumai', $this->params);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to insert a discount');
            } else {
                $this->responseData(false, null, 'fail to insert a discount');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('haumai', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to update a discount');
            } else {
                $this->responseData(false, null, 'fail to update a discount');
            }
        }
    
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('haumai', ['id' => $id], true);
            $this->responseData(true, null, 'success to delete a discount');
        } else {
            $this->responseData(false, null, 'missing params');
        }
        
        break;
}
