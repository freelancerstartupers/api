<?php
switch ($this->method) {
    case 'GET':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            $msg = 'get list of details by bill with id: '.$id;
            $sql = "select chitiethoadon.`id`, sanpham.`tensp`, chitiethoadon.`gia`, `soluong` 
                    from `chitiethoadon`, `sanpham`
                    where `mahd` = $id and sanpham.`id` = chitiethoadon.`masp` and chitiethoadon.`isdeleted` = 0";
        } else {
            $msg = 'get list of bills';
            $sql = "select `id`, `mahm`, `hotenkh`, `email`, `sdt`,
                    `diachi`,`tongtien`, `tinhtrang`, `ngaygiao`, `created` 
                    from `hoadon` 
                    where `isdeleted` = 0";
        }
        $list = load($sql);

        $this->responseData(true, $list, $msg);
        break;

    case 'POST':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $insert_id = insert('hoadon', $this->params);
            if ($insert_id > 0) {
                $this->responseData(true, null, 'success to insert a bill');
            } else {
                $this->responseData(false, null, 'fail to insert a bill');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('hoadon', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to update a bill');
            } else {
                $this->responseData(false, null, 'fail to update a bill');
            }
        }
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('hoadon', ['id' => $id ], false);
            delete('chitiethoadon', ['mahd' => $id ], false);
            $this->responseData(true, null, 'success to delete a bill');
        } else {
            $this->responseData(false, null, 'missing params');
        }

        break;
}
