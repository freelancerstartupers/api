<?php
switch ($this->method) {
    case 'GET':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        $sql = "select sanpham.`id`, `tensp`, `tenloaisp`, `maloaisp`, `dacta`, `gia`, sanpham.`hinh`, 
                `soluongviews`, `soluongban`, `giamgia`, `ngaykt`, `created` 
                from `sanpham`, `loaisanpham`
                where sanpham.`isdeleted` = 0 and loaisanpham.`id` = `maloaisp`";
        $msg = 'get list of products';
        if (!empty($id)) {
            $sql .= "and sanpham.`id` = $id";
            $msg = 'get product by id: '.$id;
        }
        $sql .= " order by sanpham.created desc";
        $list = load($sql);

        $this->responseData(true, $list, $msg);
        break;

    case 'POST':
        if (count($_FILES) > 0 && $_FILES['FILE']) {
            $relativePath = "images/".$_FILES['FILE']["name"];
            move_uploaded_file($_FILES['FILE']["tmp_name"], $relativePath);
            $this->params['hinh'] = $this->urlHost.$relativePath;
        }
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $insert_id = insert('sanpham', $this->params);
            if ($insert_id > 0) {
                $this->responseData(true, ['id' => $insert_id], 'success to insert a product');
            } else {
                $this->responseData(false, null, 'fail to insert a product');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('sanpham', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, ['id' => $id], 'success to update a product');
            } else {
                $this->responseData(false, null, 'fail to update a product');
            }
        }
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('sanpham', ['id' => $id], false);
            $this->responseData(true, null, 'success to delete a product');
        } else {
            $this->responseData(false, null, 'missing params');
        }

        break;
}
