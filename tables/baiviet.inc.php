<?php
switch ($this->method) {
    case 'GET':
        $sql = "select `id`, `noidung`, `tieude`, `media`, `duongdan`, `link` 
                from `baiviet`";
        $list = load($sql);

        $this->responseData(true, $list, 'get list of blogs');
        break;

    case 'POST':
        if (count($_FILES) > 0 && $_FILES['FILE']) {
            $relativePath = "images/".$_FILES['FILE']["name"];
            move_uploaded_file($_FILES['FILE']["tmp_name"], $relativePath);
            $this->params['duongdan'] = $this->urlHost.$relativePath;
        }
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $insert_id = insert('baiviet', $this->params);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to insert a blog');
            } else {
                $this->responseData(false, null, 'fail to insert a blog');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('baiviet', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to update a blog');
            } else {
                $this->responseData(false, null, 'fail to update a blog');
            }
        }
       
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if ($id) {
            delete('baiviet', ['id' => $id], true);
            $this->responseData(true, null, 'success to delete a blog');
        } else {
            $this->responseData(false, null, 'missing params');
        }
        
        break;
}
