<?php
switch ($this->method) {
    case 'GET':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            $msg = 'get list products by category with id: '.$id;
            $sql = "select `id`, `tensp`, `maloaisp`, `dacta`, `gia`, `hinh`,
        `soluongviews`, `soluongban`, `giamgia`, `ngaykt`, `created`
        from `sanpham`
        where `isdeleted` = 0 and `maloaisp` = $id
        and (select count(`id`) from `loaisanpham` where isdeleted = 0 and `id` = $id) > 0";
        } else {
            $msg = 'get list of categories';
            $sql = "select `id`, `tenloaisp`, `hinh`
        from `loaisanpham`
        where `isdeleted` = 0";
        }
        $list = load($sql);

        $this->responseData(true, $list, $msg);

        break;

    case 'POST':
        if (count($_FILES) > 0 && $_FILES['FILE']) {
            $relativePath = "images/".$_FILES['FILE']["name"];
            move_uploaded_file($_FILES['FILE']["tmp_name"], $relativePath);
            $this->params['hinh'] = $this->urlHost.$relativePath;
        }
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $insert_id = insert('loaisanpham', $this->params);
            if ($insert_id > 0) {
                $this->responseData(true, null, 'success to insert a category');
            } else {
                $this->responseData(false, null, 'fail to insert a category');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('loaisanpham', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to update a category');
            } else {
                $this->responseData(false, null, 'fail to update a category');
            }
        }
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('loaisanpham', ['id' => $id ], false);
            delete('sanpham', ['maloaisp' => $id ], false);
            $this->responseData(true, null, 'success to delete a category');
        } else {
            $this->responseData(false, null, 'missing params');
        }
        break;
}
