<?php
switch ($this->method) {
    case 'GET':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        $list = null;
        if (!empty($id)) {
            $msg = 'get images by product with id: '.$id;
            $sql = "select `id`, `masp`, `tenhinh`, `duongdan`
                    from `hinh`
                    where  `masp` = $id";
            $list = load($sql);
            $this->responseData(true, $list, $msg);
        } else {
            $this->responseData(false, null, 'missing params');
        }
    
        break;

    case 'POST':
        $masp = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($masp)) {
            if (count($_FILES) > 0) {
                $FILES = $_FILES;
                foreach ($FILES as $key => $value) {
                    $tenhinh = $value["name"];
                    $relativePath = "images/".$tenhinh;
                    move_uploaded_file($value["tmp_name"], $relativePath);
                    $duongdan = $this->urlHost.$relativePath;
                    $find = load("select `id` from hinh where `masp` = ".$masp." and `tenhinh` = '".$tenhinh."'");
                    if (count($find) <= 0) {
                        $insert_id = write("insert into hinh(`masp`,`tenhinh`,`duongdan`)
                                                values(".$masp.",'".$tenhinh."','".$duongdan."')");
                        if ($insert_id < 0) {
                            $this->responseData(false, null, "fail to upload a image by name = ".$tenhinh);
                        }
                    } else {
                        write("update hinh set `duongdan` = '$duongdan'
                                where `tenhinh` = '".$tenhinh."' and `masp` = ".$masp.")");
                    }
                }
                $this->responseData(true, null, 'success to upload images');
            } else {
                $this->responseData(true, null, 'there are no images for uploading');
            }
        } else {
            $this->responseData(false, null, 'missing params');
        }
        
        break;

    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('hinh', ['id' => $id ], true);
            $this->responseData(true, null, 'success to delete a image');
        } else {
            $this->responseData(false, null, 'missing params');
        }
        break;
}
