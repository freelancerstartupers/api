<?php
switch ($this->method) {
    case 'GET':
        $sql = "select `id`, `tenhienthi`, `matkhau`, `vaitro` 
                from `taikhoan`";
        $list = load($sql);

        $this->responseData(true, $list, 'get list of users');
        break;

    case 'POST':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (empty($id)) {
            $insert_id = insert('taikhoan', $this->params);
            if ($insert_id > 0) {
                $this->responseData(true, null, 'success to insert a user');
            } else {
                $this->responseData(false, null, 'fail to insert a user');
            }
        } else {
            unset($this->params[0]);
            $insert_id = update('taikhoan', $this->params, ['id' => $id]);
            if ($insert_id >= 0) {
                $this->responseData(true, null, 'success to update a user');
            } else {
                $this->responseData(false, null, 'fail to update a user');
            }
        }
        break;
    
    case 'DELETE':
        $id = isset($this->params[0]) && ctype_digit($this->params[0]) ? intval($this->params[0]) : null;
        if (!empty($id)) {
            delete('taikhoan', ['id' => $id], true);
            $this->responseData(true, null, 'success to delete a account');
        } else {
            $this->responseData(false, null, 'missing params');
        }
        break;
}
